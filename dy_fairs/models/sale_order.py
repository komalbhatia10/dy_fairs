# -*- coding: utf-8 -*-

from odoo import api, fields, models


class SaleOrder(models.Model):
    _inherit = "sale.order"

    fair_id = fields.Many2one('fair.fair', string='Fair')
    fair_booth_ids = fields.Many2many(
        'fair.booth', string="Fair Booths")
    fair_text_block = fields.Many2one('fair.text.block', string='Text Block')

    @api.multi
    def _prepare_invoice(self):
        """
        Overridden this method to add the fairs fields at the time of invoice creation.
        """
        res = super(SaleOrder, self)._prepare_invoice()
        res.update({
            'fair_id': self.fair_id.id,
            'fair_booth_ids': [(6, 0, self.fair_booth_ids.ids)],
        })
        return res
