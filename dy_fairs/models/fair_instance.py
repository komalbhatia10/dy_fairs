# -*- coding: utf-8 -*-

from odoo import api, fields, models, _


class FairInstance(models.Model):
    _name = "fair.instance"
    _description = "Fair Instance"

    name = fields.Char(string='Fair Instance', required=True, translate=True)
    fair_id = fields.Many2one('fair.fair', string='Fair')
    date_from = fields.Date(string='Start Date',
                            help='Start Date, from which the fair starts.')
    date_to = fields.Date(string='End Date',
                          help='Ending Date, the date on which the afir ends.')
    hall_ids = fields.Many2many('fair.hall', string='Hall',
                                help="The Hall which will be used for that fair.")
    fair_booth_ids = fields.Many2many(
        'fair.booth', string="Fair Booths", readonly=False)
    company_id = fields.Many2one(
        'res.company', 'Company',
        default=lambda self: self.env.user.company_id)
