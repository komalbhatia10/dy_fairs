# -*- coding: utf-8 -*-

from odoo import api, fields, models, _


class FairHall(models.Model):
    _name = "fair.hall"
    _description = "Halls"

    name = fields.Char(string='Hall Name', required=True, translate=True)
    fair_booth_ids = fields.One2many(
        'fair.booth', 'hall_id', string='Fair Booths')
    company_id = fields.Many2one(
        'res.company', 'Company',
        default=lambda self: self.env.user.company_id)
