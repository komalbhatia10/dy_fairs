# -*- coding: utf-8 -*-

from odoo import api, fields, models, _


class StockPicking(models.Model):
    _inherit = "stock.picking"

    fair_id = fields.Many2one(
        'fair.fair', string='Fair', compute="compute_fair_booths")
    fair_booth_ids = fields.Many2many(
        'fair.booth', string="Fair Booths")

    @api.multi
    def compute_fair_booths(self):
        for picking in self:
            if picking.group_id:
                picking.fair_id = picking.group_id.sale_id.fair_id
                picking.write({
                    'fair_booth_ids': [(6, 0, picking.group_id.sale_id.fair_booth_ids.ids)],
                })
