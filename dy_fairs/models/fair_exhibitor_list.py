# -*- coding: utf-8 -*-

from odoo import api, fields, models, _


class FairExhibitorList(models.Model):
    _name = "fair.exhibitor.list"
    _description = "Fair Exhibitor List"
    _rec_name = 'instance_id'

    contracting_party_id = fields.Many2one('res.partner', string='Contracting Party',
                                           help="The Name of the Contracting Party who is going to Organize this Fair.")
    fair_customer_no = fields.Char("Fair customer no")
    exhibitor_id = fields.Many2one('res.partner', string='Exhibitor Name',
                                   help="The Name of the Exhibitor who is going to Organize this Fair.")
    instance_id = fields.Many2one('fair.instance')
    hall_id = fields.Many2one('fair.hall',
                              help="The Hall which will be used for that fair.")
    fair_booth_id = fields.Many2one('fair.booth', string="Fair Booths")
    remark = fields.Char(string='Remark')
    company_id = fields.Many2one(
        'res.company', 'Company',
        default=lambda self: self.env.user.company_id)
