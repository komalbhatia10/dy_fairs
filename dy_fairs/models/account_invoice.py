# -*- coding: utf-8 -*-

from odoo import api, fields, models, _


class AccountInvoice(models.Model):

    _inherit = "account.invoice"

    fair_id = fields.Many2one('fair.fair', string='Fair')
    fair_booth_ids = fields.Many2many(
        'fair.booth', string="Fair Booths")
