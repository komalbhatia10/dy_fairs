# -*- coding: utf-8 -*-

from odoo import api, fields, models, _


class FairFair(models.Model):
    _name = "fair.fair"
    _description = "Fairs"

    name = fields.Char(string='Fair Name', required=True, translate=True)
    location = fields.Char(string='Location',
                           help="The Location where the fair is goig to be held.")
    frequency_type = fields.Selection([
        ('monthly', 'Monthly'),
        ('yearly', 'Yearly'),
    ],
        help="The Frequency at which the fair takes place.")
    organizer_id = fields.Many2one('res.partner', string='Organizer',
                                   help="The Name of the Organizer who is going to Organize this Fair.")
    company_id = fields.Many2one(
        'res.company', 'Company',
        default=lambda self: self.env.user.company_id)
