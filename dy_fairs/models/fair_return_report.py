# -*- coding: utf-8 -*-

from odoo import api, fields, models, _


class FairReturnReport(models.Model):
    _name = "fair.return.report"
    _description = "Fair Return Report"

    name = fields.Char(string='Name', required=True, translate=True)
    header = fields.Html()
    middle = fields.Html()
    bottom = fields.Html()
    company_id = fields.Many2one(
        'res.company', 'Company',
        default=lambda self: self.env.user.company_id)
