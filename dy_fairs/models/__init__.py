# -*- coding: utf-8 -*-

from . import fair_fair
from . import fair_hall
from . import fair_booth
from . import fair_instance
from . import fair_text_block
from . import fair_return_report
from . import fair_exhibitor_list
from . import sale_order
from . import stock_picking
from .import account_invoice
