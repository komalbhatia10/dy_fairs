# -*- coding: utf-8 -*-

from odoo import api, fields, models, _


class FairBooth(models.Model):
    _name = "fair.booth"
    _description = "Fair Booth"

    name = fields.Char(string='Fair Booth',
                       required=True, translate=True)
    hall_id = fields.Many2one('fair.hall', string='Hall',
                              help="The Hall for which this booth is part of.")
    instance_id = fields.Many2one('fair.instance')
    company_id = fields.Many2one(
        'res.company', 'Company',
        default=lambda self: self.env.user.company_id)
