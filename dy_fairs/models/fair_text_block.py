# -*- coding: utf-8 -*-

from odoo import api, fields, models, _


class FairTextBlock(models.Model):
    _name = "fair.text.block"
    _description = "Text Block"

    name = fields.Char(string='Name', required=True, translate=True)
    description = fields.Html(string='Descriptions')
    company_id = fields.Many2one(
        'res.company', 'Company',
        default=lambda self: self.env.user.company_id)
