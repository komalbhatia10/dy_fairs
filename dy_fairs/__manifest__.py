# -*- coding: utf-8 -*-
{
    'name': "Fairs",
    'summary': """
                    This module is used to create Fairs, Halls, Booths and manage them.
                """,

    'description': """
                    Fairs is developed to manage Fairs, Halls and Booths.
                    Customize the flow of sale order vwith respect to fairs.
                """,

    'author': "Aktiv Software",
    'website': "http://www.aktivsoftware.com/",
    'license': "",

    # Categories can be used to filter modules in modules listing
    'category': '',
    'version': '12.0.0.0.0',

    # any module necessary for this one to work correctly
    'depends': ['sale_management', 'stock',
                ],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/fair_fair_view.xml',
        'views/fair_hall_view.xml',
        'views/fair_booth_view.xml',
        'views/fair_instance_view.xml',
        'views/fair_text_block_view.xml',
        'views/fair_return_report_view.xml',
        'views/fair_exhibitor_list_view.xml',
        'views/sale_order_view.xml',
        'views/stock_picking_view.xml',
        'views/account_invoice_view.xml',
        'views/menu_view.xml',
        'report/sale_report_templates.xml',
    ],

    # only loaded in demonstration mode
    'demo': [
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
