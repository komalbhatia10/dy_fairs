Odoo 10.0 (Enterprise Edition) 

Installation 
============
* Install the Application => Apps -> Annual Statement (Technical Name: dy_annual_statement)

Annual Statement
=======================
* Economic Plan and Annual Statemnets:
    -> This menu focus on bringing all amounts together as defined in the New economic plan.

* Economic Plan:
    ->This module has been extended.
    ->It has Invoices from suppliers and consumption amounts listed, house money from WEGs is summed up and the missing or plus amount are computed.
    ->The tabs in Economic plan helps us to determine a monthly amount an owner has to pay during a year. This is done by making a forecast of costs.
    ->This module helps to calculation distribute the actual costs accordingly at the end of the term.

* Consumption:
    ->This module is same as the Building Consumption from  module building_consumption only it's menu is changed.

* Consumption Forecast Per Appartment:
    ->This model provides a list view to manually enter estimated consumption amounts per consumption,cost line, apartment and owner.
    ->When entering payments, the user has first to select  the Economic Plan,the owner and finally cost line

* Economic Plan Consumption:
    ->This model provides a list view to manually enter Actual consumption amounts per consumption,cost line, apartment and owner.

* Payments :
    ->This model will help to manually enter payments per Owner of appartment which will further calculated on economic plan in Payments per appartment Tab.

* Ep Supplier Invoices:
    ->This menu item is the replacement of the Supplier invoice in building.Ep supplier invoice is not connected in accounting.
    ->When a supplierer invoice is registered, first, the building and then the economic plan ,the cost line is to be selected.The supplier is selected from res.partner.A remark field can be edited, too.
    ->Then, invoice specific fields like amount, due date and payment date are mandatory to enter.

* Ep Owners :
    ->This is the list view of the Current owners created per economic plan.

* Ep Distribution House Money:
    ->This will display the list view of house money calculation per appartment.
    ->Various filters are added to sort the view.

* Ep Distribution Annual Statement:
    ->This model is to have one record for each owner per Economic Plan ID with all the different values
    necessary for the annual statement report.

* Annual statement:
    ->The annual statement sums up all details to compute the final amount to be rewarded or additionally paid.

* Report:
    -> Modified the calculation in existing Economic Plan Report.
    ->Annual Statement Report : It is Similar to the Economic Plan Leter, an Annual Statement Le er needs to be printed.A last page is be added with some necessary fileds.
    ->Annual Statement Excel Report : This report Overall target is to have the owner and the building manager see all computa on to understand the final amounts of the annual statement.