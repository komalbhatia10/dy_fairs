## Module dy_fairs

### 30.05.2019 
### Version 1.0.0
#### ADD
- initialer Commit
- Tabelle res.users wurde um Office 365 relevante Felder erweitert
- Unter einem Benutzer wird nun ein Reiter "Office 365 Konto" angezeigt, welcher die relevanten Felder beinhaltet
- Übersetzung wurde hinzugefügt
- Unter "Benutzer"-> Einstellungen -> Office Account Einstellungen kann die Emailadresse gesetzt werden, außerdem kann das Passwort hier eingetragen und geändert werden.